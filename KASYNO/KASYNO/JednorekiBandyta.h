#pragma once
#include "gracz.h"
#include "funkcje.h"
#include "konsola.h"

//static string symbole[10] = { "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "$" };
static char symbole[10] = { 135, 169, 164, 177, 167, 159, 140, 149, 212, 223 };
static double stawki[10] = { 1.00, 1.50, 2.00, 3.00, 4.50, 8.00, 10.00, 100.00, 150.00, 1000.00 };

void JednorekiBandyta(int* aktywnaGra, gracz* gracz1, bool* koniec);