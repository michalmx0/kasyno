#include <string>
#include <ctime>
#include <cmath>
#include "gracz.h"
#include "BlackJack.h"
#include "JednorekiBandyta.h"
#include "Ruletka.h"

double fRand(double fMin, double fMax){
	double f = (double)rand() / RAND_MAX;
	f = fMin + f * (fMax - fMin);
	f = ceil(f * 100.00) / 100.00;
	return f;
}

void ekranPowitalny() {
	cout << "Witaj w kasynie!!!";
	Sleep(2000);
	system("cls");
}

gracz wczytajGracza() {
	gracz gracz1;
	cout << "Podaj imie: ";
	cin >> gracz1.imie;
	gracz1.pula = fRand(100.00, 50000.00);
	gracz1.wygrana = 0.00;
	return gracz1;
}

int main() {
	srand(time(NULL));
	bool koniec = false;
	int aktywnaGra = 0;
	gracz gracz1;
	ekranPowitalny();
	gracz1 = wczytajGracza();
	while (!koniec) {
		system("cls");
		wyswietlInfo(gracz1, 0.0);
		if (aktywnaGra == 0) {
			cout << "1. Stanowisko do BlackJacka" << endl;
			cout << "2. Stanowisko do jednorekiego bandyty" << endl;
			cout << "3. Stanowisko do ruletki" << endl;
			cout << "4. Wyswietl TOP 100" << endl;
			cout << "5. Wyjdz z kasyna" << endl;
			wczytaj(&aktywnaGra, 1, 5, "Podaj co chcesz zrobic: ");
		}
		switch (aktywnaGra) {
		case 1:
			BlackJack(&aktywnaGra, &gracz1, &koniec);
			break;
		case 2:
			JednorekiBandyta(&aktywnaGra, &gracz1, &koniec);
			break;
		case 3:
			Ruletka(&aktywnaGra, &gracz1, &koniec);
			break;
		case 4:
			system("cls");
			cout << "TOP 100" << endl << endl;
			wyswietlTop();
			cout << endl;
			wstrzymaj("Wcisinij ENTER aby powrocic do menu...");
			aktywnaGra = 0;
			break;
		case 5:
			koniec = true;
			break;
		}
	}
	koniecGry(gracz1);
	return 0;
}