#pragma once
#include <cstdlib>
#include <string>
#include "konsola.h"

using namespace std;

int losuj(int dolnaGranica, int gornaGranica);
void wczytaj(int* co, int dolnaGranica, int gornaGranica, string komunikat);
void wczytajKwote(double* kwota, double dolnaGranica, double gornaGranica, string komunikat);
bool porownaj(double liczba1, double liczba2);
void wstrzymaj(string komunikat);