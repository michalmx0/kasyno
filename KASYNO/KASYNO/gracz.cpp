#include "gracz.h"

void wyswietlInfo(gracz gracz1, double kwotaZakladu) {
	cout << "Imie: " << gracz1.imie << endl;
	cout << "Pula: " << setprecision(2) << fixed << gracz1.pula + gracz1.wygrana - kwotaZakladu << "$" << endl;
	cout << "Wygrana: " << setprecision(2) << fixed << gracz1.wygrana << "$" << endl << endl;
}

void koniecGry(gracz gracz1) {
	system("cls");
	if (porownaj(gracz1.pula, -gracz1.wygrana)) {
		cout << "Przegrales wszystko :(";
	}
	else if (gracz1.wygrana < 0) {
		cout << "Przegrales " << -gracz1.wygrana << "$" << endl;
		cout << "Wychodzisz z kasyna z kwota " << gracz1.pula + gracz1.wygrana << "$" << endl;
	}
	else {
		cout << "Wygrales " << gracz1.wygrana << "$" << endl;
		cout << "Wychodzisz z kasyna z kwota " << gracz1.pula + gracz1.wygrana << "$" << endl;
		zapiszWynik(gracz1);
	}
}

bool czyKoniec(gracz gracz1) {
	if (porownaj(gracz1.pula, -gracz1.wygrana) || gracz1.pula + gracz1.wygrana < 1.0) {
		cout << "Nie mozesz juz kontynuowac gry." << endl;
		wstrzymaj("Wcisnij ENTER aby kontynuowac...");
		return true;
	}
	else {
		return false;
	}
}

gracz* zmienRozmiar(gracz* tab, int* rozmiar) {
	(*rozmiar)++;
	gracz* nowa = new gracz[*rozmiar];
	for (int j = 0; j < *rozmiar - 1; j++) {
		nowa[j] = tab[j];
	}
	delete[] tab;
	return nowa;
}

gracz* pobierzTop(int *rozmiar) {
	ifstream plik;
	plik.open("wyniki.txt");
	*rozmiar = 0;

	gracz* wyniki = NULL;
	while (plik.good()) {
		wyniki = zmienRozmiar(wyniki, rozmiar);
		plik >> wyniki[*rozmiar - 1].imie;
		plik >> wyniki[*rozmiar - 1].wygrana;
	}

	plik.close();
	return wyniki;
}

void wyswietlTop() {
	int rozmiar;
	gracz* wyniki = pobierzTop(&rozmiar);
	for (int i = 0; i < rozmiar; i++) {
		cout << i+1 << ". " << wyniki[i].imie << " " << wyniki[i].wygrana << "$" << endl;
	}
}

void zapiszWynik(gracz gracz1) {
	int rozmiar, i = 0;
	bool znaleziono = false;
	gracz* wyniki = pobierzTop(&rozmiar);
	while (i < rozmiar && !znaleziono) {
		if (wyniki[i].imie == gracz1.imie) {
			znaleziono = true;
		}
		else {
			i++;
		}
	}
	
	if (znaleziono && wyniki[i].wygrana < gracz1.wygrana) {
		wyniki[i].wygrana = gracz1.wygrana;
	}
	else {
		wyniki = zmienRozmiar(wyniki, &rozmiar);
		wyniki[rozmiar - 1] = gracz1;
	}

	for (i = 0; i < rozmiar; i++) {
		for (int j = 0; j < rozmiar - 1; j++) {
			if (wyniki[j].wygrana < wyniki[j + 1].wygrana) {
				gracz temp = wyniki[j];
				wyniki[j] = wyniki[j + 1];
				wyniki[j + 1] = temp;
			}
		}
	}

	ofstream plik;
	int ile = rozmiar;
	if (ile > 100) {
		ile = 100;
	}
	plik.open("wyniki.txt");
	for (i = 0; i < ile; i++) {
		plik << wyniki[i].imie << " " << wyniki[i].wygrana;
		if (i != rozmiar - 1) {
			plik << endl;
		}
	}
}