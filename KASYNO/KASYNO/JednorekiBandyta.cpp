#include "JednorekiBandyta.h"

void JednorekiBandyta(int* aktywnaGra, gracz* gracz1, bool* koniec) {
	system("cls");
	double kwota = 10.00;
	bool gra = true, wygrana = false;
	int decyzja; // dalszaGra
	string linia = "\n";
	wyswietlInfo(*gracz1, 0.0);
	cout << endl;
	cout << "JEDNOREKI BANDYTA" << endl << endl;
	if (gracz1->pula + gracz1->wygrana < 10) {
		cout << "Masz na koncie mniej niz 10$. Nie mozesz juz zagrac kolejnej gry." << endl;
		wstrzymaj("Wcisnij ENTER aby przejsc do menu...");
		*aktywnaGra = 0;
	}
	else {
		int wspY = pobierzKursorY();
		wczytaj(&decyzja, 0, 1, "Chcesz postawic 10$ zeby rozegrac 1 gre? (1 - TAK, 0 - NIE)\n");
		if (decyzja == 1) {
			wyczyscWiersz(wspY);
			wyczyscWiersz(wspY + 1);
			for (int i = 0; i < 120; i++) {
				przejdzDoXY({ 0, wspY });
				cout << symbole[losuj(0, 9)] << " " << symbole[losuj(0,9)] << " " << symbole[losuj(0, 9)];
				Sleep(i);
			}
			int wylosowane[3];
			for (int i = 0; i < 3; i++) {
				wylosowane[i] = losuj(0, 9);
			}
			przejdzDoXY({ 0, wspY });
			cout << symbole[wylosowane[0]] << " " << symbole[wylosowane[1]] << " " << symbole[wylosowane[2]] << endl << endl;
			if (wylosowane[0] == wylosowane[1] && wylosowane[1] == wylosowane[2]) {
				kwota *= stawki[wylosowane[0]];
				cout << "Wygrales " << kwota << "$" << endl << endl;
				gracz1->wygrana += kwota;
			}
			else {
				cout << "Przegrales " << kwota << "$" << endl << endl;
				gracz1->wygrana -= kwota;
			}
			wstrzymaj("Wcisnij ENTER aby kontynuowac...");
		}
		else {
			*aktywnaGra = 0;
		}

		*koniec = czyKoniec(*gracz1);
	}
}