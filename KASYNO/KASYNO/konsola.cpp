#include "konsola.h"

void przejdzDoXY(wspolrzedne wsp) {
	COORD p = { wsp.x, wsp.y };
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), p);
}

wspolrzedne pobierzKursor() {
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	COORD result;
	wspolrzedne wsp;
	if (!GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi)) {
		return { -1, -1 };
	}
	wsp.x = csbi.dwCursorPosition.X;
	wsp.y = csbi.dwCursorPosition.Y;
	return wsp;
}

int pobierzKursorY() {
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	COORD result;
	if (!GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi)) {
		return -1;
	}
	return csbi.dwCursorPosition.Y;
}

int pobierzLiczbeKolumn() {
	CONSOLE_SCREEN_BUFFER_INFO csbi;

	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	return csbi.srWindow.Right - csbi.srWindow.Left + 1;
}

void wyczyscWiersz(int posY) {
	przejdzDoXY({ 0, posY });
	for (int i = 0; i < pobierzLiczbeKolumn(); i++) {
		cout << " ";
	}
	przejdzDoXY({ 0, posY });
}

void ustawKolor(int kolor) {
	HANDLE hOut;
	hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hOut, kolor | FOREGROUND_INTENSITY);
}

void resetujKolor() {
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
}
