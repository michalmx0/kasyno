#pragma once
#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include "funkcje.h"

using namespace std;

typedef struct gracz {
	string imie;
	double pula;
	double wygrana;
} gracz;

void wyswietlInfo(gracz gracz1, double kwotaZakladu);
void koniecGry(gracz gracz1);
bool czyKoniec(gracz gracz1);
void zapiszWynik(gracz gracz1);
void wyswietlTop();