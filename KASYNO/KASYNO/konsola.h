#pragma once
#include <Windows.h>
#include <iostream>

using namespace std;

typedef struct wspolrzedne {
	int x;
	int y;
} wspolrzedne;

void przejdzDoXY(wspolrzedne wsp);
wspolrzedne pobierzKursor();
int pobierzKursorY();
int pobierzLiczbeKolumn();
void wyczyscWiersz(int posY);
void ustawKolor(int kolor);
void resetujKolor();