#pragma once
#include "konsola.h"
#include "funkcje.h"
#include "gracz.h"

using namespace std;

static int mnoznik[7] = { 1, 2, 5, 8, 11, 17, 35 };
static char kolory[37] = { 'g', 'r', 'b', 'r', 'b', 'r', 'b', 'r', 'b', 'r', 'b', 'b', 'r', 'b', 'r', 'b', 'r', 'b', 'r', 'r', 'b', 'r', 'b', 'r', 'b', 'r', 'b', 'r', 'b', 'b', 'r', 'b', 'r', 'b', 'r', 'b', 'r' };

typedef struct pole {
	char kolor;
	int liczba;
} pole;

void Ruletka(int* aktywnaGra, gracz* gracz1, bool* koniec);