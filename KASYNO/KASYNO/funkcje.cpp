#include "funkcje.h"
#include <limits>

int losuj(int dolnaGranica, int gornaGranica) {
	return rand() % (gornaGranica - dolnaGranica + 1) + dolnaGranica;
}

bool porownaj(double liczba1, double liczba2) {
	if (fabs(liczba1 - liczba2) < 0.001) {
		return true;
	}
	return false;
}

void wczytaj(int* co, int dolnaGranica, int gornaGranica, string komunikat) {
	wspolrzedne wsp = pobierzKursor();
	cout << komunikat;
	cin >> *co;
	//sprawdzic czy pobieramy prawidolowa wartosc int
	while (*co<dolnaGranica || *co>gornaGranica) {
		cin.clear();
		cin.ignore(INT_MAX, '\n');
		cout << "Podano bledna opcje!!!";
		system("pause");
		wyczyscWiersz(wsp.y);
		wyczyscWiersz(wsp.y + 1);
		przejdzDoXY(wsp);
		cout << komunikat;
		cin >> *co;
	}
}

void wczytajKwote(double* kwota, double dolnaGranica, double gornaGranica, string komunikat) {
	wspolrzedne wsp = pobierzKursor();
	cout << komunikat;
	cin >> *kwota;
	while (*kwota<dolnaGranica || *kwota>gornaGranica+0.001) {
		cin.clear();
		cin.ignore(INT_MAX, '\n');
		cout << "Podano bledna kwote!!! ";
		system("pause");
		wyczyscWiersz(wsp.y);
		wyczyscWiersz(wsp.y + 1);
		przejdzDoXY(wsp);
		cout << komunikat;
		cin >> *kwota;
	}
}

void wstrzymaj(string komunikat) {
	cout << komunikat;
	cin.ignore(INT_MAX, '\n');
	cin.clear();
	cin.get();
}