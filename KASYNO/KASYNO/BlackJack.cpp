#include "BlackJack.h"

int sumujKarty() {
	int suma = 0;
	for (int i = 0; i < 13; i++) {
		suma += ilosci[i];
	}
	return suma;
}

string losujKarte(int *suma, int *liczba) {
	int indeks = losuj(0, 12);
	while (ilosci[indeks] <= 0) {
		indeks = losuj(0, 12);
	}
	ilosci[indeks]--;
	*suma += wartosci[indeks];
	(*liczba)++;
	return talia[indeks];
}

void wypiszKarte(string karta) {
	wspolrzedne wsp = pobierzKursor();
	przejdzDoXY({ wsp.x, wsp.y - 2 });
	cout << " ___";
	przejdzDoXY({ wsp.x, wsp.y - 1 });
	cout << "|   |";
	przejdzDoXY(wsp);
	cout << "|";
	if (karta != "10") {
		cout << " " << karta;
	}
	else {
		cout << karta;
	}
	cout << " |";
	przejdzDoXY({ wsp.x, wsp.y + 1 });
	cout << "|___|";
}

void nowaKarta(wspolrzedne *wsp, int *suma, int *liczba) {
	wsp->x = wsp->x + 6;
	przejdzDoXY(*wsp);
	wypiszKarte(losujKarte(suma, liczba));
}

void BlackJack(int* aktywnaGra, gracz* gracz1, bool *koniec) {
	system("cls");
	double kwota;
	bool gra = true, wygrana = false;
	int decyzja; // dalszaGra; 
	int sumaTwoja = 0, sumaKrupiera = 0;
	int liczbaTwoichKart = 0, liczbaKartKrupiera = 0;
	wspolrzedne wspTwoichKart, wspKartKrupiera;
	wyswietlInfo(*gracz1, 0.0);
	cout << endl;
	cout << "BLACK JACK" << endl << endl;
	wczytajKwote(&kwota, 1, gracz1->pula + gracz1->wygrana, "Podaj kwote do obstawienia: ");
	wyczyscWiersz(pobierzKursorY() - 1);
	cout << "Postawiona kwota " << kwota << "$" << endl;
	cout << endl << endl;
	cout << "Karty na stole ";
	wspKartKrupiera = pobierzKursor();
	wypiszKarte(losujKarte(&sumaKrupiera, &liczbaKartKrupiera));
	cout << endl << endl << endl;
	cout << "Twoje karty ";
	wspTwoichKart = pobierzKursor();
	wypiszKarte(losujKarte(&sumaTwoja, &liczbaTwoichKart));
	while (gra) {
		for (int i = 1; i < 6; i++) {
			wyczyscWiersz(wspTwoichKart.y + i + 2);
		}
		przejdzDoXY(wspTwoichKart);
		cout << endl << endl << endl;
		cout << "Co chcesz zrobic?" << endl;
		cout << "1. Dobierz karte" << endl;
		cout << "2. Pas" << endl;
		wczytaj(&decyzja, 1, 2, "");
		switch (decyzja) {
		case 1:
			if (liczbaTwoichKart < 8) {
				nowaKarta(&wspTwoichKart, &sumaTwoja, &liczbaTwoichKart);
				if (sumaTwoja >= 21) {
					for (int i = 1; i < 6; i++) {
						wyczyscWiersz(wspTwoichKart.y + i + 2);
					}
					przejdzDoXY(wspTwoichKart);
					if ((liczbaTwoichKart == 2 && sumaTwoja == 22) || (sumaTwoja == 21 && sumaKrupiera != 11)) {
						gracz1->wygrana += kwota;
						cout << endl << endl << endl << "Wygrales " << kwota << "$" << endl;
					}
					else {
						gracz1->wygrana -= kwota;
						cout << endl << endl << endl << "Przegrales " << kwota << "$" << endl;
					}
					gra = false;
				}
			}
			else {
				cout << "Nie mozesz dobrac wiecej kart." << endl;
				decyzja = 2;
			}
			break;
		case 2:
			gra = false;
			przejdzDoXY(wspTwoichKart);
			for (int i = 1; i < 6; i++) {
				wyczyscWiersz(wspTwoichKart.y + i + 2);
			}
			while (sumaKrupiera <= sumaTwoja && sumaKrupiera < 22 && liczbaKartKrupiera < 8) {
				nowaKarta(&wspKartKrupiera, &sumaKrupiera, &liczbaKartKrupiera);
				Sleep(2000);
			}
			przejdzDoXY(wspTwoichKart);
			if ((liczbaKartKrupiera == 2 && sumaKrupiera == 22) || (sumaKrupiera <= 21 && sumaKrupiera > sumaTwoja)) {
				gracz1->wygrana -= kwota;
				cout << endl << endl << endl << "Przegrales " << kwota << "$" << endl;
			}
			else {
				gracz1->wygrana += kwota;
				cout << endl << endl << endl << "Wygrales " << kwota << "$" << endl;
			}
			break;
		}
	}
	*koniec = czyKoniec(*gracz1);

	if (!*koniec) {
		wczytaj(aktywnaGra, 0, 1, "Chcesz rozegrac kolejna gre? (1 - TAK, 0 - NIE)\n");
		if (*aktywnaGra == 1 && sumujKarty() < 8) {
			for (int i = 0; i < 13; i++) {
				ilosci[i] = 4;
			}
			cout << "Nowa talia. " << endl;
			system("pause");
		}
	}
	
}