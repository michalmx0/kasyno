#include "Ruletka.h"

void wyswietlZaklad(double pojedynczy[], double podwojny[], int* liczbowe, int ile) {
	wspolrzedne wsp1[6], wsp2[6], wsp, koniec;
	przejdzDoXY({ 28, pobierzKursorY() });
	ustawKolor(BACKGROUND_GREEN);
	cout << "        0         " << endl;
	resetujKolor();
	wsp = pobierzKursor();
	for (int i = 0; i < 6; i++) {
		wsp1[i].x = 4;
		wsp1[i].y = wsp.y + (i*3) + 1;
		wsp2[i].x = 17;
		wsp2[i].y = wsp.y + (i*6) + 1;
	}
	cout << "1 do 18" << "    " << "Pierwsza 12" << endl;
	cout << endl << endl;
	cout << "Nieparzyste" << endl;
	cout << endl << endl;
	cout << "Czerwone" << "    " << "Druga 12" << endl;
	cout << endl << endl;
	cout << "Czarne" << endl;
	cout << endl << endl;
	cout << "Parzyste" << "    " << "Trzecia 12" << endl;
	cout << endl << endl;
	cout << "19 do 36" << endl;
	cout << endl;
	koniec = pobierzKursor();
	wsp.x = 28;
	int c = 0;
	for (int i = 1; i < 36; i=i+3)
	{
		przejdzDoXY({ 28, wsp.y + i / 3 + c});
		for (int j = 0; j < 3; j++) {
			if (kolory[i+j] == 'r') {
				ustawKolor(BACKGROUND_RED);
			}
			else {
				resetujKolor();
			}
			cout << "  " << i+j;
			if (i + j < 10) {
				cout << " ";
			}
			cout << "  ";
		}
		if (i == 10 || i == 22) {
			c = c + 2;
		}
	}
	resetujKolor();
	przejdzDoXY({ 28, koniec.y });
	cout << " 2do1  2do1  2do1 " << endl;

	for (int i = 0; i < 6; i++) {
		if (pojedynczy[i] > 0) {
			przejdzDoXY(wsp1[i]);
			cout << "X";
		}
	}
	for (int i = 0; i < 3; i++) {
		if (podwojny[i] > 0) {
			przejdzDoXY(wsp2[i]);
			cout << "X";
		}
	}
	for (int i = 0; i < ile; i++) {
		if (liczbowe[i] == 0) {
			przejdzDoXY({ 38, wsp.y - 1 });
			ustawKolor(BACKGROUND_GREEN);
			cout << "X";
		}
		else {
			//int dodajDoX = 5 * (liczbowe[i] % 4);
			int dodajDoX = 17;
			int dodajDoY = liczbowe[i] / 3;
			if (liczbowe[i] % 3 == 1) {
				dodajDoX = 5;
			}
			else if (liczbowe[i] % 3 == 2) {
				dodajDoX = 11;
			}
			else {
				dodajDoY -= 1;
			}
			if (liczbowe[i] > 24) {
				dodajDoY += 4;
			}
			else if (liczbowe[i] > 12) {
				dodajDoY += 2;
			}
			przejdzDoXY({ 28 + dodajDoX , wsp.y + dodajDoY });
			if (kolory[liczbowe[i]] == 'r') {
				ustawKolor(BACKGROUND_RED);
			}
			else {
				resetujKolor();
			}
			cout << "X";
		}
	}
	resetujKolor();
	for (int i = 3; i < 6; i++) {
		if (podwojny[i] > 0) {
			przejdzDoXY({ 31 + 6 * (i - 3), koniec.y+1 });
			cout << "X";
		}
	}
	koniec.y += 3;
	przejdzDoXY(koniec);
}

int* zmienRozmiar(int *tab, int *rozmiar) {
	(*rozmiar)++;
	int* nowa = new int[*rozmiar];
	for (int j = 0; j < *rozmiar - 1; j++) {
		nowa[j] = tab[j];
	}
	delete[] tab;
	return nowa;
}

bool czyDodany(int* liczbowe, int rozmiar, int liczba) {
	for (int i = 0; i < rozmiar; i++) {
		if (liczbowe[i] == liczba) {
			return true;
		}
	}
	return false;
}

int *wczytajZaklad(gracz gracz1, double pojednyczy[], double podwojny[], int *liczbowe, int *rozmiar, double *kwotaLiczbowe, double *kwotaZakladu, bool *zaklad, int* aktywnaGra, int *decyzja) {
	cout << "Wybierz co chcesz zrobic:" << endl;
	cout << "1. Numery 1-18" << endl;
	cout << "2. Nieparzyste" << endl;
	cout << "3. Czerwone" << endl;
	cout << "4. Czarne" << endl;
	cout << "5. Parzyste" << endl;
	cout << "6. Numery 19-36" << endl;
	cout << "7. Tuzin" << endl;
	cout << "8. Numer (max 6 numerow)" << endl;
	cout << "9. Kolumna" << endl;
	cout << "10. Zakoncz zaklad i przejdz do gry" << endl;
	cout << "11. Wroc do menu" << endl << endl;
	wczytaj(decyzja, 1, 11, "Wybierz opcje: ");
	if (*decyzja > 0 && *decyzja < 7) {
		if (pojednyczy[*decyzja - 1] > 0) {
			cout << "Obstawiles/as juz ta opcje!" << endl;
			wstrzymaj("Wcisnij ENTER aby kontynuowac...");
		}
		else {
			wczytajKwote(&pojednyczy[*decyzja - 1], 1, gracz1.pula + gracz1.wygrana - *kwotaZakladu, "Podaj kwote zakladu: ");
			*kwotaZakladu += pojednyczy[*decyzja - 1];
		}
	}
	if (*decyzja == 7 || *decyzja == 9) {
		if (*decyzja == 7) {
			wczytaj(decyzja, 1, 3, "Podaj, ktory tuzin (1-3): ");
			if (podwojny[*decyzja - 1] > 0) {
				cout << "Obstawiles/as juz ta opcje!" << endl;
				wstrzymaj("Wcisnij ENTER aby kontynuowac...");
			}
			else {
				wczytajKwote(&podwojny[*decyzja - 1], 1, gracz1.pula + gracz1.wygrana - *kwotaZakladu, "Podaj kwote zakladu: ");
				*kwotaZakladu += podwojny[*decyzja - 1];
			}
		}
		else {
			wczytaj(decyzja, 1, 3, "Podaj nr kolumny (1-3): ");
			if (podwojny[3 + *decyzja - 1] > 0) {
				cout << "Obstawiles/as juz ta opcje!" << endl;
				wstrzymaj("Wcisnij ENTER aby kontynuowac...");
			}
			else {
				wczytajKwote(&podwojny[3 + *decyzja - 1], 1, gracz1.pula + gracz1.wygrana - *kwotaZakladu, "Podaj kwote zakladu: ");
				*kwotaZakladu += podwojny[3 + *decyzja - 1];
			}
		}
	}
	if (*decyzja == 8) {
		if (*rozmiar < 6) {
			int numer;
			wczytaj(&numer, 0, 36, "Podaj numer: ");
			if (!czyDodany(liczbowe, *rozmiar, numer)) {
				liczbowe = zmienRozmiar(liczbowe, rozmiar);
				liczbowe[*rozmiar - 1] = numer;
				if (porownaj(*kwotaLiczbowe, 0.0)) {
					wczytajKwote(kwotaLiczbowe, 1, gracz1.pula + gracz1.wygrana - *kwotaZakladu, "Podaj kwote zakladu: ");
					*kwotaZakladu += *kwotaLiczbowe;
				}
			}
			else {
				cout << "Obstawiles/as juz ten numer!" << endl;
				wstrzymaj("Wcisinij ENTER aby kontynuowac...");
			}
			
		}
		else {
			cout << "Obstawiles/as juz 6 numerow!" << endl;
			wstrzymaj("Wcisinij ENTER aby kontynuowac...");
		}
	}
	if (*decyzja == 10) {
		if (porownaj(*kwotaZakladu, 0)) {
			cout << "Nie mozesz rozpoczac gry bez obstawienia zakladu!" << endl;
			wstrzymaj("Wcisinij ENTER aby kontynuowac...");
		}
		else {
			*zaklad = false;
		}
	}
	if (*decyzja == 11) {
		if (*kwotaZakladu > 0.0) {
			cout << "Postawiles pieniadze i nie mozesz teraz wrocic do menu!" << endl;
			wstrzymaj("Wcisinij ENTER aby kontynuowac...");
		}
		else {
			*zaklad = false;
			*aktywnaGra = 0;
		}
	}
	return liczbowe;
}

bool sprawdzWygrana(gracz* gracz1, double pojedynczy[], double podwojny[], int* liczbowe, double kwotaLiczbowe, int rozmiar, int numer) {
	double wygranaKwota = 0.0;
	for (int i = 0; i < 6; i++) {
		if (pojedynczy[i] > 0.0) {
			bool wygrana = false;
			if (i == 0 && numer > 0 && numer < 19) {
				wygrana = true;
			}
			else if (i == 1 && numer % 2 == 1) {
				wygrana = true;
			}
			else if (i == 2 && kolory[numer] == 'r') {
				wygrana = true;
			}
			else if (i == 3 && kolory[numer] == 'b') {
				wygrana = true;
			}
			else if (i == 4 && numer % 2 == 0) {
				wygrana = true;
			}
			else if (i == 5 && numer > 18 && numer < 37) {
				wygrana = true;
			}
			if (wygrana) {
				gracz1->wygrana += mnoznik[0] * pojedynczy[i];
				wygranaKwota += mnoznik[0] * pojedynczy[i];
			}
			else {
				gracz1->wygrana -= pojedynczy[i];
				wygranaKwota -= pojedynczy[i];
			}
		}
		if (podwojny[i] > 0.0) {
			bool wygrana = false;
			if (i >= 0 && i < 3) {
				if ((numer % 12 != 0 && numer / 12 == i) || (numer % 12 == 0 && numer / 12 == i + 1)) {
					wygrana = true;
				}
			}
			if (i > 2 && i < 6) {
				if (numer % 3 == i - 2 && i < 5) {
					wygrana = true;
				}
				else if (numer % 3 == 0 && i == 5) {
					wygrana = true;
				}
			}
			if (wygrana) {
				gracz1->wygrana += mnoznik[1] * podwojny[i];
				wygranaKwota += mnoznik[1] * podwojny[i];
			}
			else {
				gracz1->wygrana -= podwojny[i];
				wygranaKwota -= podwojny[i];
			}
		}
	}
	if (kwotaLiczbowe > 0.0) {
		if (czyDodany(liczbowe, rozmiar, numer)) {
			int indeks = 7 - rozmiar;
			if (rozmiar == 6) {
				indeks = 2;
			}
			gracz1->wygrana += mnoznik[indeks] * kwotaLiczbowe;
			wygranaKwota += mnoznik[indeks] * kwotaLiczbowe;
		}
		else {
			gracz1->wygrana -= kwotaLiczbowe;
			wygranaKwota -= kwotaLiczbowe;
		}
	}
	if (wygranaKwota > 0.0) {
		cout << "Wygrales " << wygranaKwota << "$" << endl;
	}
	else {
		cout << "Przegrales " << -wygranaKwota << "$" << endl;
	}
	wstrzymaj("Wcisnij ENTER aby kontynuowac...");
	return czyKoniec(*gracz1);
}

void Ruletka(int* aktywnaGra, gracz* gracz1, bool* koniec) {
	double pojedynczy[6] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
	double podwojny[6] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
	int rozmiar = 0, decyzja;
	int* liczbowe = new int[rozmiar];
	double kwotaLiczbowe = 0.0;
	double kwotaZakladu = 0.0;
	bool zaklad = true;
	while (zaklad) {
		system("cls");
		wyswietlInfo(*gracz1, kwotaZakladu);
		cout << endl;
		cout << "RULETKA" << endl << endl;
		wyswietlZaklad(pojedynczy, podwojny, liczbowe, rozmiar);
		cout << "Calkowita postawiona kwota " << kwotaZakladu << "$" << endl << endl;
		liczbowe = wczytajZaklad(*gracz1, pojedynczy, podwojny, liczbowe, &rozmiar, &kwotaLiczbowe, &kwotaZakladu, &zaklad, aktywnaGra, &decyzja);
	}
	if (decyzja == 10) {
		system("cls");
		wyswietlInfo(*gracz1, kwotaZakladu);
		cout << endl;
		cout << "RULETKA" << endl << endl;
		wyswietlZaklad(pojedynczy, podwojny, liczbowe, rozmiar);
		cout << "Calkowita postawiona kwota " << kwotaZakladu << "$" << endl << endl;
		wspolrzedne wsp = pobierzKursor();
		int numer;
		for (int i = 0; i < 120; i++) {
			przejdzDoXY({ 0, wsp.y });
			numer = losuj(0, 36);
			if (kolory[numer] == 'g') {
				ustawKolor(BACKGROUND_GREEN);
			}
			else if (kolory[numer] == 'r') {
				ustawKolor(BACKGROUND_RED);
			}
			else {
				resetujKolor();
			}
			cout << "  " << numer << "  ";
			if (numer < 10) {
				cout << " ";
			}
			Sleep(i);
		}
		resetujKolor();
		cout << endl;
		*koniec = sprawdzWygrana(gracz1, pojedynczy, podwojny, liczbowe, kwotaLiczbowe, rozmiar, numer);
	}
}